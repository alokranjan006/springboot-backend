package com.crudproject.springbootbackend;

import com.crudproject.springbootbackend.model.Employee;
import com.crudproject.springbootbackend.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootBackendApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootBackendApplication.class, args);
	}

	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public void run(String... args) throws Exception {
		Employee employee = new Employee();
		employee.setFirstName("Alok");
		employee.setLastName("Ranjan");
		employee.setEmailID("alok@gmail.com");
		employeeRepository.save(employee);

		Employee employee1 = new Employee();
		employee1.setFirstName("Ram");
		employee1.setLastName("Sharma");
		employee1.setEmailID("ram@gmail.com");
		employeeRepository.save(employee1);
	}
}
